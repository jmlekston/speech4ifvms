# Speech4ifvms #
Speech synthesis and recognition at the service of interactive fiction. This prototoype is based on desktop chrome speech synthesis and recognition capabilities and ifvms a interactive fiction Z virtual engine writen in  javascript.

## Installation ##
installation is pretty simple, you only need node js and git installed

```
#!shell

git clone https://jmlekston@bitbucket.org/jmlekston/speech4ifvms.git
npm install
npm start
```

## Playing ##
The prototype use chrome inner speech capabilities (executed localy). Open chrome and go to https://localhost. Bypass warning and accept the cert. This warning is due to the fact we are using a self signed certificat to initiate the secured connection 'htts'. We use a secured connection to avoid the boring microphone acceptance popup (it will be shown only one time).
Select a story from the folder resource at this time only lostpig.z8.json is working. 

## Limitations ##
At this time only outtext and input text ifvms features are  implemented. Only one voice is fully supported. The speech recognition requiere a GOOD microphone and a quiet place