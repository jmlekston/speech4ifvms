var fs = require( 'fs' );
var readline = require('readline');
var iconv = require( 'iconv-lite' );
var jquery = require('./jquery.min.js');
var say = require('say');
var ZVM = require( './dist/zvm.js' );

var data = iconv.decode( fs.readFileSync( './tests/LostPig.z8' ), 'latin1' );
var vm = new ZVM();
var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

var text_to_say = [];

// Convert text into an array
function text_to_array( text, array )
{
	var i = 0, l;
	array = array || [];
	for ( l = text.length % 8; i < l; ++i )
	{
		array.push( text.charCodeAt( i ) );
	}
	for ( l = text.length; i < l; )
	{
		// Unfortunately unless text is cast to a String object there is no shortcut for charCodeAt,
		// and if text is cast to a String object, it's considerably slower.
		array.push( text.charCodeAt( i++ )
			, text.charCodeAt( i++ )
			, text.charCodeAt( i++ )
			, text.charCodeAt( i++ )
			, text.charCodeAt( i++ )
			, text.charCodeAt( i++ )
			, text.charCodeAt( i++ )
			, text.charCodeAt( i++ )
		);
	}
	return array;
}

function manage(orders, start_index)
{
	for(index = start_index; index < orders.length; index++)
	{
	    current_order = orders[index];
	    //console.log(current_order);

	    code = current_order.code;
	    if (code === 'stream')
	    {
	      if (current_order.to === 'status' || current_order.text === undefined || current_order.text === '') continue;
	      
	      console.log(current_order.text);
	      say.speak(null,current_order.text, function()
	      	{
	      		manage(orders, index+1);
	      	});
	      return;
	    }
	    else if (code === 'read')
	    {
	      rl.question(">",function(answer) {
	        rl.close();
	        current_order.response = answer;
	        vm.inputEvent(current_order);

	      });
	    }
	}
}

vm.outputEvent = function(orders)
{
	manage(orders, 0);
};

vm.inputEvent({
	code: 'load',
	data: text_to_array( data )
});

vm.restart();
vm.run();



