
var ZVM = require( '../node_modules/ifvms/dist/zvm.js' );
var jquery = require('../node_modules/jquery/dist/jquery.min.js');

if (window.File 
  && window.FileReader 
  && window.FileList 
  && window.Blob 
  && window.speechSynthesis
  && window.webkitSpeechRecognition) {
  // Great success! All the File APIs are supported.
} else {
  alert('The File APIs are not fully supported in this browser.');
}


function speech(content, next_callback)
{
  content = content.trim();
	if (content.match(/(\.|\,|\!|\?|\>)/) || content === '') 
	{
		setTimeout(function() {
			next_callback();
		},0);
		return;
	}
  console.log("'"+content+"'");
  var msg = new SpeechSynthesisUtterance(content.toString());

  msg.lang = 'en-US';
  //msg.lang = 'fr-FR';
  
  msg.rate = 0.7;
  msg.pitch = 1.0;
  msg.volume = 1.0;
  var voices = window.speechSynthesis.getVoices();
  msg.voice = voices.filter(function(voice) { return voice.name == 'Fred'; })[0];

  msg.addEventListener('end', function () {
    next_callback();
  });

  console.log(msg); //IMPORTANT!! Do not remove: Logging the object out fixes some onend firing issues.
  //placing the speak invocation inside a callback fixes ordering and onend issues.
  setTimeout(function () {
      window.speechSynthesis.speak(msg);
  }, 0);
}

function tell(bunches, index, end_of_tell_callback)
{
  if (index < bunches.length)
  {
    speech(bunches[index], (function(new_index)
    {
      return function()
      {
        tell(bunches, new_index, end_of_tell_callback);        
      }
    })(index+1));
    return;
  }
  end_of_tell_callback();
}

function start_recognition()
{
  var recognition = new webkitSpeechRecognition();
  recognition.continuous = true;
  recognition.interimResults = true;
  recognition.lang = 'en-UK';
  recognition.onresult = function()
  {
    var final_transcript = jquery('#command_content').val();
    for (var i = event.resultIndex; i < event.results.length; ++i) 
    {
      if (event.results[i].isFinal) 
      {
        final_transcript += event.results[i][0].transcript;
        recognition.stop();
        jquery('#command_content').val(final_transcript);
        jquery('#command').submit();    
      } 
    }          
  };
  recognition.start();
}

function resume(vm, orders, start_index)
{
  while (true)
  {
    for (var index = start_index; index < orders.length; index++)
    {
      current_order = orders[index];
      var code = current_order.code;
      if (code === 'stream')
      {
        if (current_order.to === 'status') continue;

        var content = current_order.text || '';
        jquery('div').text(content);
        if (content === '') continue;


        var bunches = content.toString().split(/(\.|\,|\!|\?|\>)/);
        tell(bunches, 0, (function(new_index) 
        {
          return function() 
          {
            console.log(new_index + "end");
            resume(vm, orders, new_index);
          }
        })(index+1)) ;

        return;
      }
      else if (code === 'find')
      {
        continue;
      }
      else if (code === 'read')
      {
        request_order = current_order;
        jquery('#command_content').val('');
        start_recognition();
        return;
      }
      else
      {
        return;
      }
    }
  }
}


jquery(document).ready(
  function()
  {
    var vm = new ZVM();

    jquery('#story_file').change(
      function(event)
      {
        var file = this.files[0];
        var file_reader = new FileReader();
        file_reader.onload = function(event)
        {
          var file_content = event.target.result;
          var data = jquery.parseJSON(file_content);
          vm.inputEvent(
            {
              code: 'load',
              data: data
            }
          );
          vm.restart();
          vm.run();
          resume(vm, vm.orders,0);
        }
        file_reader.readAsText(file);
      }
    );
    jquery('#command').submit(
      function(event)
      {
        var command_text = jquery('#command_content').val();
        request_order.response = command_text;
        vm.inputEvent(request_order);
        resume(vm, vm.orders,0);
        event.preventDefault();
        return null;
      }
    );

    window.speechSynthesis.onvoiceschanged = function() {
      voices = window.speechSynthesis.getVoices();
      console.log(voices);
    };
  }
);



