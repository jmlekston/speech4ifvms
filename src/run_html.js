
var ZVM = require( './dist/zvm.js' );
var jquery = require('./jquery.min.js');

if (window.File && window.FileReader && window.FileList && window.Blob) {
  // Great success! All the File APIs are supported.
} else {
  alert('The File APIs are not fully supported in this browser.');
}

var orders;
var current_order;

function cycle(vm)
{
  while (true)
  {
    var orders = vm.orders;
    for (var index = 0; index < orders.length; index++)
    {
      current_order = orders[index];
      console.log(current_order);
      var code = current_order.code;
      if (code === 'stream')
      {
        if (current_order.to === 'status') continue;

        var content = current_order.text || '';
        jquery('div').text(content);
/*
       	var msg = new SpeechSynthesisUtterance(content);
        msg.onend(
          function(event)
          {
            resume()
          }
        );
        speechSynthesis.speak(msg);
*/

      }
      else if (code === 'find')
      {
        continue;
      }
      else if (code === 'read')
      {
        request_order = current_order;
        jquery('#command_content').val('');
        return;
      }
      else
      {
        return;
      }
    }
  }
}


jquery(document).ready(
  function()
  {
    var vm = new ZVM();

    jquery('#story_file').change(
      function(event)
      {
        var file = this.files[0];
        var file_reader = new FileReader();
        file_reader.onload = function(event)
        {
          var file_content = event.target.result;
          var data = jquery.parseJSON(file_content);
          vm.inputEvent(
            {
              code: 'load',
              data: data
            }
          );
          vm.restart();
          vm.run();
          cycle(vm);
        }
        file_reader.readAsText(file);
      }
    );
    jquery('#command').submit(
      function(event)
      {
        var command_text = jquery('#command_content').val();
        console.log(command_text);
        request_order.response = command_text;
        vm.inputEvent(request_order);
        cycle(vm);
        event.preventDefault();
        return null;
      }
    );
  }
);



