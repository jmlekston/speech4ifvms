var say = require('say');

// no callback, fire and forget
say.speak(null, 'whats up, dog?');

// use default voice in System Preferences
say.speak(null, 'Hello!');

// no callback, fire and forget
say.speak(null, 'whats up, dog?');

// output some text to the console as the callback
say.speak(null, 'whats up, dog?', function () {
     console.log('text to speech complete');
});


// try using translate.js with say.js
var translate = require('translate');

translate.text('Yo quero tacos por favor', function(result){
     say.speak(null, result);
});