var fs = require( 'fs' );
var iconv = require( 'iconv-lite' );
function text_to_array( text, array )
{
	var i = 0, l;
	array = array || [];
	for ( l = text.length % 8; i < l; ++i )
	{
		array.push( text.charCodeAt( i ) );
	}
	for ( l = text.length; i < l; )
	{
		// Unfortunately unless text is cast to a String object there is no shortcut for charCodeAt,
		// and if text is cast to a String object, it's considerably slower.
		array.push( text.charCodeAt( i++ ), text.charCodeAt( i++ ), text.charCodeAt( i++ ), text.charCodeAt( i++ ),
			text.charCodeAt( i++ ), text.charCodeAt( i++ ), text.charCodeAt( i++ ), text.charCodeAt( i++ ) );
	}
	return array;
}

var path = process.argv[2];

var content = fs.readFileSync( path );
var decoded_content = iconv.decode( content, 'latin1' );
var array_content  = text_to_array(decoded_content);
var json_content = JSON.stringify(array_content);
process.stdout.write(json_content);
