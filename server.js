var fs = require('fs');
var express = require('express');
var https = require('https');
var key = fs.readFileSync('./cert/testhttps-key.pem');
var cert = fs.readFileSync('./cert/testhttps-cert.pem');
var https_options = 
{
    key: key,
    cert: cert
};


var app = express();
https.createServer(https_options, app).listen(443);

app.use(express.static(__dirname));


var server = app.listen(process.env.npm_package_config_port, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('Listening at http://%s:%s', host, port);
});
